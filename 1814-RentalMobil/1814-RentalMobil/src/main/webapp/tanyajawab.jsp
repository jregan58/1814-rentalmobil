<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<%@ page import = "java.util.*" %>
<%@ page import = "java.io.*" %>
<%@ page import = "java.io.FileInputStream" %>
<%@ page import = "java.io.IOException" %>
<%@ page import = "java.io.PrintWriter" %>
<%@ page import = "com.google.auth.oauth2.ServiceAccountCredentials" %>
<%@ page import = "com.google.cloud.Timestamp" %>
<%@ page import = "com.google.cloud.datastore.Datastore" %>
<%@ page import = "com.google.cloud.datastore.DatastoreOptions" %>
<%@ page import = "com.google.cloud.datastore.Entity" %>
<%@ page import = "com.google.cloud.datastore.FullEntity" %>
<%@ page import = "com.google.cloud.datastore.IncompleteKey" %>
<%@ page import = "com.google.cloud.datastore.KeyFactory" %>
<%@ page import = "com.google.cloud.datastore.Query" %>
<%@ page import = "com.google.cloud.datastore.QueryResults" %>
<%@ page import = "com.google.cloud.datastore.StructuredQuery.CompositeFilter" %>
<%@ page import = "com.google.cloud.datastore.StructuredQuery.OrderBy" %>
<%@ page import = "com.google.cloud.datastore.StructuredQuery.PropertyFilter" %>
<%@ page import = "com.google.appengine.api.datastore.Text" %>
<%@ page import = "com.google.cloud.Date" %>

<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml" lang="en">
  <head>
    <meta http-equiv="content-type" content="application/xhtml+xml; charset=UTF-8" />
        <link rel="stylesheet" type="text/css" href="bootstrap.css">
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
  		<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
    <link rel="stylesheet" type="text/css" href="hd.css">

    <title>Rental Mobil</title>
     <div class="header">Rental Mobil</div>
    <ul class="nav nav-tabs" role="tablist">
	  <li><a href="home.jsp">Home</a></li>
      <li><a href="user.jsp">Daftar user</a></li>
	  <li><a href="tanyajawab.jsp">Tanya Jawab</a></li>
	    <li><a href="tambahmobil.jsp">Tambah mobil</a></li>
	  <li><a href="login.jsp" class="navlogin">Login</a></li>
      <li><a href="register.jsp" class="navregister">Register</a></li>
      <li><a href="rental.jsp">Rental</a></li>
	</ul>
    
    <%
    try{
     Cookie ck[]=request.getCookies(); %>
     	<%if(ck[0].getValue()!=""){ %>
	     <div class="containerlogin">
	     <form action="home.jsp">  
	     <p>Welcome : <%= ck[0].getValue()%>&nbsp&nbsp<button class="btn btn-default" name="btnLogout" type="submit">   
	            <span class="">Logout</span>
	          </button>
	    </form>
	     </div>
     	<%} %>
    <%} 
    catch(Exception e)
    {
    	
    }
    
    %> 
    
   
    </div>
    
    <br>
    
  </head>

  <body>
  <%!
		//ArrayList<Artikel> listartikel = new ArrayList<Artikel>();	
  String pesan="";
	%>
	  
  <% 
              if (request.getParameter("btnTanya")!=null)
              {
            	  
      		    %>
      		  	<div class="navbar-form navbar-left" id="container">
      		  	
      		  	 		<form class="form-inline" action='tanyajawab.jsp' method="POST">
					  
					    <div id="legend">
					      <legend class="">&nbspBuat Pertanyaan</legend>
					    </div>
					    
					    <div class="control-group">
					      <!-- Judul Artikel -->
					      <label class="control-label"  for="Judul Artikel">Judul Pertanyaan</label>
					      <div class="controls">
					         <textarea rows="1" cols="60" name="judulpertanyaan" placeholder="Judul Artikel" class="input-register"></textarea>
					      </div>
					    </div>
					    
					    <br>
					    					   
					    
					    <div class="control-group">
					      <!--Isi Artikel -->
					      <label class="control-label"  for="Isi Artikel">Isi Pertanyaan</label>
					      <div class="controls">
					        <textarea rows="4" cols="60" name="isipertanyaan" placeholder="Isi Artikel" class="input-register"></textarea>
					      </div>
					      <br>
					      <button class="btn btn-outline-success my-2 my-sm-0" name="btnPertanyaan" id="btnpsize1" type="submit">Buat Pertanyaan</button>
					      <br>
					      <p class="pesanreg"><%= pesan%></p>
					    </div>
						</form>
						<br>											 
		</div>
					 	<br>
					    				                  
      		  		
                  <%
                  
              }
              
              else if (request.getParameter("btnPertanyaan")!=null)
				{
	            	  	Cookie ck[]=request.getCookies();
	             		//out.println(ck[0].getValue());
            		     pesan="Insert Berhasil !";
	     					String judulpertanyaan=request.getParameter("judulpertanyaan");			
	     	                  Datastore datastore = DatastoreOptions.newBuilder().setProjectId("proyek-222615").setCredentials(ServiceAccountCredentials.fromStream(new FileInputStream("proyek-35e790d3f268.json"))).build().getService();    

	     					//Datastore datastore = DatastoreOptions.newBuilder().setProjectId("cloud-computing-182310").setCredentials(ServiceAccountCredentials.fromStream(new FileInputStream("Cloud-Computing-22c9e0cf1dd7.json"))).build().getService();    
	     					// Insert
	     				    KeyFactory keyFactory = datastore.newKeyFactory().setKind("tanyajawab");	  
	     				    IncompleteKey key = keyFactory.newKey();	
	     				    FullEntity<IncompleteKey> newTask = FullEntity.newBuilder(key)
	     				            .set("judulpertanyaan", judulpertanyaan)
	     				           .set("user1", ck[0].getValue())
	     				           .set("user2", "")
	     				            .set("isipertanyaan", request.getParameter("isipertanyaan"))
	     				           .set("created", Timestamp.now())
	     				           .set("jawabanpertanyaan", "").build();
	     				    datastore.add(newTask);		 
	     					
	     				    //Datastore datastore = DatastoreOptions.newBuilder().setProjectId("cloud-computing-182310").setCredentials(ServiceAccountCredentials.fromStream(new FileInputStream("Cloud-Computing-22c9e0cf1dd7.json"))).build().getService(); 
	     			      
	     			      /* Query<Entity> query = Query.newEntityQueryBuilder()
	     			              .setKind("tanyajawab")
	     			              .setFilter(CompositeFilter.and(
	     			                  PropertyFilter.eq("judulpertanyaan", judulpertanyaan)))                          
	     			              .build();
	     			      QueryResults<Entity> results = datastore.run(query);
     					
     			      		if (results.hasNext()) {
     			 	     	Entity entity = results.next();
     			     	    Entity updatedEntity = Entity.newBuilder(entity).set("img", entity.getKey().getId()).build();
     			     	    datastore.update(updatedEntity);
     			     	    //out.println("Update done!");
     			    		 } */
            	  		
	     				   response.sendRedirect("tanyajawab.jsp");  
      
			  }
  			  
              else if (request.getParameter("btnJawab")!=null)
              {
      		  	  
            	  
            	  
            	  
            	  
            	  
              }
  
              else if(request.getParameter("btnLogout")!=null)
              {
            	  try{
            	  Cookie ck=new Cookie("userlogin","");//deleting value of cookie  
            	  ck.setMaxAge(0);//changing the maximum age to 0 seconds  
            	  response.addCookie(ck);//adding cookie in the response
            	  }
            	  catch(Exception e)
            	  {
            		  
            	  }
            	  response.sendRedirect("home.jsp");  
              }
              else{
            	  
            	  //listartikel.clear();
            	                    Datastore datastore = DatastoreOptions.newBuilder().setProjectId("proyek-222615").setCredentials(ServiceAccountCredentials.fromStream(new FileInputStream("proyek-35e790d3f268.json"))).build().getService();    

            	  //Datastore datastore = DatastoreOptions.newBuilder().setProjectId("cloud-computing-182310").setCredentials(ServiceAccountCredentials.fromStream(new FileInputStream("Cloud-Computing-22c9e0cf1dd7.json"))).build().getService();
            	  Query<Entity> query = Query.newEntityQueryBuilder().setKind("tanyajawab").setOrderBy(OrderBy.desc("created")).build();
      		    QueryResults<Entity> results = datastore.run(query);
      		    %>
      		  	<div class="navbar-form navbar-left" id="container">
      		  	
      		  	 	<div id="legend">
			      	<legend class="">&nbspTanya Jawab</legend>
			    	</div>
      		  	
      		  	 <nav class="navbar navbar-light bg-light" id="tbtanyajawab">
				  <form class="form-inline" action="tanyajawab.jsp" method="POST">
					<button class="btn btn-outline-success my-2 my-sm-0" name="btnTanya" id="btnpsize1" type="submit">Buat Pertanyaan</button>
				    <button class="btn btn-outline-success my-2 my-sm-0" name="btnJawab" id="btnpsize2" type="submit">Jawab Pertanyaan</button>
				    <br>
				    <br>
					<input class="form-control mr-sm-2" type="search" id="tbpertanyaan2" name="tbJudulPertanyaan" placeholder="Judul Pertanyaan" aria-label="Search">
				  </form>
				</nav>
      		  	<div id="legend">
			      	<legend class=""></legend>
			    	</div>
      		  	<%
      		    while (results.hasNext()) {
      		        Entity entity = results.next(); 
      		        %>
      		       <!--  /* out.println("Judul Artikel : " + entity.getString("judulartikel") +"<br/>"
                              + " "+"Isi Artikel : "+ entity.getString("isiartikel")+"<br/>"
                              + " "+"Tanggal Insert :"+(entity.getTimestamp("created").toSqlTimestamp().getDate())+"-"+entity.getTimestamp("created").toSqlTimestamp().getMonth()+"-"+(entity.getTimestamp("created").toSqlTimestamp().getYear() + 1900)+"<br/>"
                              + " "+"Kategori: "+ entity.getString("kategori")+"<br/>"
                              + " "+"ID Artikel : "+ entity.getKey().getId()+"<br/>"
                              );     */
                              //listartikel.add(new Artikel(entity.getString("judulartikel"))); -->                       
                       <div id="namapenyakit"><%=entity.getString("judulpertanyaan")%></div>
                       <div id="namapenyakit"><%=entity.getString("isipertanyaan")%></div>
                       <div id="namapenyakit">Dipostkan Oleh : <%=entity.getString("user1")%></div>
                       <br>                               			  			          
                    <%}%>
                    
                    
                    
                    <%-- <%for (int i = 0; i < listartikel.size(); i++) { %>
                    	<h2>Judul Artikel <%=listartikel.get(i).getJudul()%></h2>  
                    	<form action="home.jsp" method="POST">
                    	<button type="button" onclick="window.location.href='home.jsp'" class="btn btn-link" name="btn(<%=i%>)"><h2><%=listartikel.get(i).getJudul()%></h2></button>                 	                   	
                    	</form>
                    	<br>
                    	
                    	<%
                    		/* if(request.getParameter("btn("+i+")")!=null)
                    		{
                    			response.sendRedirect("login.jsp");  
                    		} */
                    		Entity entity = results.next();
                    	%>
                    	
                    <%} %> --%>
                    
                    
                    
      		  		</div>
              <%}%>
  
  
  
    <br>
    <br>
     
  
    
    
    
  <div class="footer">
   <p>&copy; 2018 Rental Mobil <br>
  All Rights Reserved.</p>
 </div>
  </body>
</html>
