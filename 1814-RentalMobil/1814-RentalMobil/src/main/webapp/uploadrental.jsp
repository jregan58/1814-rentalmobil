<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<%@ page import = "java.io.FileInputStream" %>
<%@ page import = "java.io.IOException" %>
<%@ page import = "java.io.PrintWriter" %>
<%@ page import = "com.google.auth.oauth2.ServiceAccountCredentials" %>
<%@ page import = "com.google.cloud.Timestamp" %>
<%@ page import = "com.google.cloud.datastore.Datastore" %>
<%@ page import = "com.google.cloud.datastore.DatastoreOptions" %>
<%@ page import = "com.google.cloud.datastore.Entity" %>
<%@ page import = "com.google.cloud.datastore.FullEntity" %>
<%@ page import = "com.google.cloud.datastore.IncompleteKey" %>
<%@ page import = "com.google.cloud.datastore.KeyFactory" %>
<%@ page import = "com.google.cloud.datastore.Query" %>
<%@ page import = "com.google.cloud.datastore.QueryResults" %>
<%@ page import = "com.google.cloud.datastore.StructuredQuery.CompositeFilter" %>
<%@ page import = "com.google.cloud.datastore.StructuredQuery.OrderBy" %>
<%@ page import = "com.google.cloud.datastore.StructuredQuery.PropertyFilter" %>
<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml" lang="en">
<head>
    <meta http-equiv="content-type" content="application/xhtml+xml; charset=UTF-8" />
    		 <link rel="stylesheet" type="text/css" href="bootstrap.css">
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
  		<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
    <link rel="stylesheet" type="text/css" href="hd.css">
    
     <div class="header">Rental Mobil</div>
    <ul class="nav nav-tabs" role="tablist">
	  <li><a href="home.jsp">Home</a></li>
      <li><a href="user.jsp">Daftar user</a></li>
	  <li><a href="tanyajawab.jsp">Tanya Jawab</a></li>
	    <li><a href="tambahmobil.jsp">Tambah mobil</a></li>
	  <li><a href="login.jsp" class="navlogin">Login</a></li>
      <li><a href="register.jsp" class="navregister">Register</a></li>
	<li><a href="rental.jsp">Rental</a></li>
		<li><a href="uploadrental.jsp">Upload Rental</a></li>
	</ul>
	
	
</head>
<body>
<% 
	if (request.getParameter("btnupload")!=null)
	{
		
	}
              
%>
	<form action = 'uploadrental.jsp' method = 'post'>
		<input type="text" id="namamobil" name="namamobil" placeholder="nama mobil" class="input-register"><br>
		<select name="jenismobil">
		  <option value="suv">suv</option>
		  <option value="mpv">mpv</option>
		  <option value="sedan">sedan</option>
 		  <option value="city car">city car</option>
		</select>
		<br>
		<select name="transmisi">
		  <option value="at">AT</option>
		  <option value="mt">MT</option>
		</select>
		<br>
		<select name="bahanbakar">
		  <option value="solar">solar</option>
		  <option value="bensin">bensin</option>
		</select>
		<br>
		<input type = "submit" id="btnupload" name="btnupload" value="upload">
	</form>
</body>
</html>