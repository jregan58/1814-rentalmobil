<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<%@ page import = "java.io.FileInputStream" %>
<%@ page import = "java.io.IOException" %>
<%@ page import = "java.io.PrintWriter" %>
<%@ page import = "com.google.auth.oauth2.ServiceAccountCredentials" %>
<%@ page import = "com.google.cloud.Timestamp" %>
<%@ page import = "com.google.cloud.datastore.Datastore" %>
<%@ page import = "com.google.cloud.datastore.DatastoreOptions" %>
<%@ page import = "com.google.cloud.datastore.Entity" %>
<%@ page import = "com.google.cloud.datastore.FullEntity" %>
<%@ page import = "com.google.cloud.datastore.IncompleteKey" %>
<%@ page import = "com.google.cloud.datastore.KeyFactory" %>
<%@ page import = "com.google.cloud.datastore.Query" %>
<%@ page import = "com.google.cloud.datastore.QueryResults" %>
<%@ page import = "com.google.cloud.datastore.StructuredQuery.CompositeFilter" %>
<%@ page import = "com.google.cloud.datastore.StructuredQuery.OrderBy" %>
<%@ page import = "com.google.cloud.datastore.StructuredQuery.PropertyFilter" %>

<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml" lang="en">
  <head>
    <meta http-equiv="content-type" content="application/xhtml+xml; charset=UTF-8" />
    		 <link rel="stylesheet" type="text/css" href="bootstrap.css">
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
  		<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
    <link rel="stylesheet" type="text/css" href="hd.css">
    <title>Halo Dokter</title>
     <div class="header">Rental Mobil</div>
    <ul class="nav nav-tabs" role="tablist">
	  <li><a href="home.jsp">Home</a></li>
	   <li><a href="user.jsp">Daftar user</a></li>
	  <li><a href="tanyajawab.jsp">Tanya Jawab</a></li>
	    <li><a href="tambahmobil.jsp">Tambah mobil</a></li>
	  <li><a href="login.jsp" class="navlogin">Login</a></li>
      <li><a href="register.jsp" class="navregister">Register</a></li>  
      <li><a href="rental.jsp">Rental</a></li>
	</ul>
	 <%
    try{
     Cookie ck[]=request.getCookies(); %>
     	<%if(ck[0].getValue()!=""){ %>
	     <div class="containerlogin">
	     <form action="home.jsp">
	     <%ck[0].setValue("abedzzzzzzz");%>  
	     <p>Welcome : <%= ck[0].getValue()%>&nbsp&nbsp<button class="btn btn-default" name="btnLogout" type="submit">   
	            <span class="">Logout</span>
	          </button>
	    </form>
	     </div>
     	<%} %>
     	<% if(ck[0].getValue()== ""){
     		response.sendRedirect("login.jsp");   
     	}%>
    <%} 
    catch(Exception e)
    {
    	
    }
    
    %> 
	<br> 
  </head>

  <body>
      <%! 
      	String pesan="";
      %>
      
      <%  
		if (request.getParameter("btnRegister")!=null)
		{
			Cookie ck[]=request.getCookies();
			pesan="Tambah mobil Berhasil !";
			//String username=request.getParameter("username");
			//String password=request.getParameter("password");
			String namalengkap=request.getParameter("namalengkap");
			String email=request.getParameter("email");
			String platnomor=request.getParameter("platnomor");
			String jenis=request.getParameter("jenismobil");
			String transmisi=request.getParameter("transmisi");
			String bahanbakar=request.getParameter("bahanbakar");
			
			Datastore datastore = DatastoreOptions.newBuilder().setProjectId("proyek-222615").setCredentials(ServiceAccountCredentials.fromStream(new FileInputStream("proyek-35e790d3f268.json"))).build().getService();    
			// Insert
		    KeyFactory keyFactory = datastore.newKeyFactory().setKind("mobil");	  
		    IncompleteKey key = keyFactory.newKey();	
		    FullEntity<IncompleteKey> newTask = FullEntity.newBuilder(key)
		           .set("namamobil", namalengkap).set("email", ck[0].getValue()).set("platnomor",platnomor).set("jenismobil" , jenis).set("transmisi" , transmisi).set("bahanbakar" , bahanbakar).build();
		    datastore.add(newTask);		 
			   	    

		}
		else
		{
			pesan="";
		}
		
	%>
  	<form class="form-register" action='tambahmobil.jsp' method="POST">
  <fieldset>
    <div id="legend">
      <legend class="">&nbspHalaman tambah mobil</legend>
    </div>
    
    <div class="control-group">
      <!-- Username -->
      <!-- <label class="control-label"  for="username">Username</label>
      <div class="controls">
        <input type="text" id="username" name="username" placeholder="Username" class="input-register">
        <p class="help-block">Username can contain any letters or numbers, without spaces</p>
      </div> -->
    </div>
    
    <div class="control-group">
      <!-- nama lengkap -->
      <label class="control-label"  for="Nama Lengkap">Nama Mobil</label>
      <div class="controls">
        <input type="text" id="Nama Lengkap" name="namalengkap" placeholder="Nama Mobil" class="input-register">
        <br>
		<input type="text" id="platnomor" name="platnomor" placeholder="platnomor" class="input-register">
        <br>
        <select name="jenismobil">
		  <option value="suv">suv</option>
		  <option value="mpv">mpv</option>
		  <option value="sedan">sedan</option>
 		  <option value="city car">city car</option>
		</select>
		<br>
		<select name="transmisi">
		  <option value="at">AT</option>
		  <option value="mt">MT</option>
		</select>
		<br>
		<select name="bahanbakar">
		  <option value="solar">solar</option>
		  <option value="bensin">bensin</option>
		</select>
		<br>      
      </div>
    </div>
 
    
 	
    <div class="control-group">
      <!-- Button -->
      <div class="controls">
        <button class="btn btn-success" name="btnRegister">Post</button>       
      </div>
    </div>
	<br>
	 <div class="control-group">
		<p class="pesanreg"><%= pesan%></p>
		</div>
  </fieldset>
</form>
	
	<br>
<div class="footer">
  <p>&copy; 2017 Halo Dokter <br>
  All Rights Reserved.</p>
</div>

<br>
<p class="pesanreg"></p>
<br>
<br>
<br>

	
  </body>
</html>