<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<%@ page import = "java.io.FileInputStream" %>
<%@ page import = "java.io.IOException" %>
<%@ page import = "java.io.PrintWriter" %>
<%@ page import = "com.google.auth.oauth2.ServiceAccountCredentials" %>
<%@ page import = "com.google.cloud.Timestamp" %>
<%@ page import = "com.google.cloud.datastore.Datastore" %>
<%@ page import = "com.google.cloud.datastore.DatastoreOptions" %>
<%@ page import = "com.google.cloud.datastore.Entity" %>
<%@ page import = "com.google.cloud.datastore.FullEntity" %>
<%@ page import = "com.google.cloud.datastore.IncompleteKey" %>
<%@ page import = "com.google.cloud.datastore.KeyFactory" %>
<%@ page import = "com.google.cloud.datastore.Query" %>
<%@ page import = "com.google.cloud.datastore.QueryResults" %>
<%@ page import = "com.google.cloud.datastore.StructuredQuery.CompositeFilter" %>
<%@ page import = "com.google.cloud.datastore.StructuredQuery.OrderBy" %>
<%@ page import = "com.google.cloud.datastore.StructuredQuery.PropertyFilter" %>

<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml" lang="en">
  <head>
    <meta http-equiv="content-type" content="application/xhtml+xml; charset=UTF-8" />
    		 <link rel="stylesheet" type="text/css" href="bootstrap.css">
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
  		<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
    <link rel="stylesheet" type="text/css" href="hd.css">
    <title>Halo Dokter</title>
     <div class="header">Rental Mobil</div>
    <ul class="nav nav-tabs" role="tablist">
	  <li><a href="home.jsp">Home</a></li>
	   <li><a href="user.jsp">Daftar user</a></li>
	  <li><a href="tanyajawab.jsp">Tanya Jawab</a></li>
	    <li><a href="tambahmobil.jsp">Tambah mobil</a></li>
	  <li><a href="login.jsp" class="navlogin">Login</a></li>
      <li><a href="register.jsp" class="navregister">Register</a></li>  
      <li><a href="rental.jsp">Rental</a></li>
	</ul>
	 <%
    try{
     Cookie ck[]=request.getCookies(); %>
     	<%
     	if(ck[0].getValue()!=""){ 
     		response.sendRedirect("home.jsp");   	    	
     	}
     	%>
	     
    <%} 
    catch(Exception e)
    {
    	
    }
    
    %> 
	<br> 
  </head>

  <body>
      <%! 
      	String pesan="";
      %>
      
      <%  
		if (request.getParameter("btnRegister")!=null)
		{
			pesan="Register Berhasil !";
			//String username=request.getParameter("username");
			String password=request.getParameter("password");
			String namalengkap=request.getParameter("namalengkap");
			String email=request.getParameter("email");
			Datastore datastore = DatastoreOptions.newBuilder().setProjectId("proyek-222615").setCredentials(ServiceAccountCredentials.fromStream(new FileInputStream("proyek-35e790d3f268.json"))).build().getService();    
			// Insert
		    KeyFactory keyFactory = datastore.newKeyFactory().setKind("user");	  
		    IncompleteKey key = keyFactory.newKey();	
		    FullEntity<IncompleteKey> newTask = FullEntity.newBuilder(key)
		           .set("password", password).set("namalengkap", namalengkap).set("email", email).build();
		    datastore.add(newTask);		 
			   	    

		}
		else
		{
			pesan="";
		}
		
	%>
  	<form class="form-register" action='register.jsp' method="POST">
  <fieldset>
    <div id="legend">
      <legend class="">&nbspRegister Page</legend>
    </div>
    
    <div class="control-group">
      <!-- Username -->
      <!-- <label class="control-label"  for="username">Username</label>
      <div class="controls">
        <input type="text" id="username" name="username" placeholder="Username" class="input-register">
        <p class="help-block">Username can contain any letters or numbers, without spaces</p>
      </div> -->
    </div>
    
    <div class="control-group">
      <!-- nama lengkap -->
      <label class="control-label"  for="Nama Lengkap">Nama Lengkap</label>
      <div class="controls">
        <input type="text" id="Nama Lengkap" name="namalengkap" placeholder="Nama Lengkap" class="input-register">
        <p class="help-block">Nama Lengkap can contain any letters or numbers</p>
      </div>
    </div>
 
    <div class="control-group">
      <!-- E-mail -->
      <label class="control-label" for="email">E-mail</label>
      <div class="controls">
        <input type="text" id="email" name="email" placeholder="E-mail" class="input-register" pattern="[a-z0-9._%+-]+@[a-z0-9.-]+\.[a-z]{2,3}$">
        <p class="help-block">Please provide your E-mail</p>
      </div>
    </div>
 
    <div class="control-group">
      <!-- Password-->
      <label class="control-label" for="password">Password</label>
      <div class="controls">
        <input type="password" id="password" name="password" placeholder="Password" class="input-register" pattern=".{4,}">
        <p class="help-block">Password should be at least 4 characters</p>
      </div>
    </div>
 
    <div class="control-group">
      <!-- Password -->
      <label class="control-label"  for="password_confirm">Password (Confirm)</label>
      <div class="controls">
        <input type="password" id="password_confirm" name="password_confirm" placeholder="Password (Confirm)" class="input-register">
        <p class="help-block">Please confirm password</p>
      </div>
    </div>
 	
    <div class="control-group">
      <!-- Button -->
      <div class="controls">
        <button class="btn btn-success" name="btnRegister">Register</button>       
      </div>
    </div>
	<br>
	 <div class="control-group">
		<p class="pesanreg"><%= pesan%></p>
		</div>
  </fieldset>
</form>
	
	<br>
<div class="footer">
  <p>&copy; 2017 Halo Dokter <br>
  All Rights Reserved.</p>
</div>

<br>
<p class="pesanreg"></p>
<br>
<br>
<br>

	
  </body>
</html>