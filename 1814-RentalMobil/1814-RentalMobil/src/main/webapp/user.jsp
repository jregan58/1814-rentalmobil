<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<%@ page import = "java.io.FileInputStream" %>
<%@ page import = "java.io.IOException" %>
<%@ page import = "java.io.PrintWriter" %>
<%@ page import = "com.google.auth.oauth2.ServiceAccountCredentials" %>
<%@ page import = "com.google.cloud.Timestamp" %>
<%@ page import = "com.google.cloud.datastore.Datastore" %>
<%@ page import = "com.google.cloud.datastore.DatastoreOptions" %>
<%@ page import = "com.google.cloud.datastore.Entity" %>
<%@ page import = "com.google.cloud.datastore.FullEntity" %>
<%@ page import = "com.google.cloud.datastore.IncompleteKey" %>
<%@ page import = "com.google.cloud.datastore.KeyFactory" %>
<%@ page import = "com.google.cloud.datastore.Query" %>
<%@ page import = "com.google.cloud.datastore.QueryResults" %>
<%@ page import = "com.google.cloud.datastore.StructuredQuery.CompositeFilter" %>
<%@ page import = "com.google.cloud.datastore.StructuredQuery.OrderBy" %>
<%@ page import = "com.google.cloud.datastore.StructuredQuery.PropertyFilter" %>

<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml" lang="en">
  <head>
    <meta http-equiv="content-type" content="application/xhtml+xml; charset=UTF-8" />
    		 <link rel="stylesheet" type="text/css" href="bootstrap.css">
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
  		<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
    <link rel="stylesheet" type="text/css" href="hd.css">
    <title>Rental Mobil</title>
     <div class="header">Rental Mobil</div>
    <ul class="nav nav-tabs" role="tablist">
	  <li><a href="home.jsp">Home</a></li>
	   <li><a href="user.jsp">Daftar user</a></li>
	  <li><a href="tanyajawab.jsp">Tanya Jawab</a></li>
	    <li><a href="tambahmobil.jsp">Tambah mobil</a></li>
	  <li><a href="login.jsp" class="navlogin">Login</a></li>
      <li><a href="register.jsp" class="navregister">Register</a></li>
      <li><a href="rental.jsp">Rental</a></li>  
	</ul>
	 <%
    try{
     Cookie ck[]=request.getCookies(); %>
     	<%
     	if(ck[0].getValue()!=""){ 
     		//response.sendRedirect("home.jsp");   	    	
     	}
     	%>
	     
    <%} 
    catch(Exception e)
    {
    	
    }
    
    %> 
	<br> 
  </head>

  <body>
      
      <% 
              if (request.getParameter("btnSearch")!=null)
              {
                  String cari = request.getParameter("tbSearch");
                  Datastore datastore = DatastoreOptions.newBuilder().setProjectId("proyek-222615").setCredentials(ServiceAccountCredentials.fromStream(new FileInputStream("proyek-35e790d3f268.json"))).build().getService();    
                   
                  Query<Entity> query = Query.newEntityQueryBuilder()
                          .setKind("user")
                          .setFilter(CompositeFilter.and(
                              PropertyFilter.eq("namalengkap", cari)))                          
                          .build();
                  QueryResults<Entity> results = datastore.run(query);
                  
                  %>
        		  <div class="navbar-form navbar-left" id="container">
      		  	
      		  	 	<div id="legend">
			      	<legend class="">&nbspCari Informasi user</legend>
			    	</div>
      		  	
      		  	 <nav class="navbar navbar-light bg-light" id="tbobat">
				  <form class="form-inline">
				    <input class="form-control mr-sm-2" type="search" id="tbobat2" name="tbSearch" placeholder="Search User" aria-label="Search">
				    <button class="btn btn-outline-success my-2 my-sm-0" name="btnSearch" type="submit">Cari</button>
				  </form>
				</nav>
      		  	<div id="legend">
			      	<legend class=""></legend>
			    	</div>
      		  	<%
      		    while (results.hasNext()) {
      		        Entity entity = results.next(); 
      		        %>
      		       <!--  /* out.println("Judul Artikel : " + entity.getString("judulartikel") +"<br/>"
                              + " "+"Isi Artikel : "+ entity.getString("isiartikel")+"<br/>"
                              + " "+"Tanggal Insert :"+(entity.getTimestamp("created").toSqlTimestamp().getDate())+"-"+entity.getTimestamp("created").toSqlTimestamp().getMonth()+"-"+(entity.getTimestamp("created").toSqlTimestamp().getYear() + 1900)+"<br/>"
                              + " "+"Kategori: "+ entity.getString("kategori")+"<br/>"
                              + " "+"ID Artikel : "+ entity.getKey().getId()+"<br/>"
                              );     */
                              //listartikel.add(new Artikel(entity.getString("judulartikel"))); -->
                      
                       <br> 
                       <br>                      
                       <div id="namaobat"> Nama : <%=entity.getString("namalengkap")%></div>
                       <div id="legend">
			      			<legend class=""></legend>
			    		</div>
                      <div id="namaobat"> Email : <%=entity.getString("email")%></div>
                       <div id="legend">
			      			<legend class=""></legend>
			    		</div>
			    		 
                      		          
                    <%}%>
                    
                    
                    
                    <%-- <%for (int i = 0; i < listartikel.size(); i++) { %>
                    	<h2>Judul Artikel <%=listartikel.get(i).getJudul()%></h2>  
                    	<form action="home.jsp" method="POST">
                    	<button type="button" onclick="window.location.href='home.jsp'" class="btn btn-link" name="btn(<%=i%>)"><h2><%=listartikel.get(i).getJudul()%></h2></button>                 	                   	
                    	</form>
                    	<br>
                    	
                    	<%
                    		/* if(request.getParameter("btn("+i+")")!=null)
                    		{
                    			response.sendRedirect("login.jsp");  
                    		} */
                    		Entity entity = results.next();
                    	%>
                    	
                    <%} %> --%>
                    
                    
                    
      		  		</div>
                  <%
                  
              }
             
              else if(request.getParameter("btnLogout")!=null)
              {
            	  try{
            	  Cookie ck=new Cookie("userlogin","");//deleting value of cookie  
            	  ck.setMaxAge(0);//changing the maximum age to 0 seconds  
            	  response.addCookie(ck);//adding cookie in the response
            	  }
            	  catch(Exception e)
            	  {
            		  
            	  }
            	  response.sendRedirect("home.jsp");  
              }
              else{
            	  //listartikel.clear();
            	   Datastore datastore = DatastoreOptions.newBuilder().setProjectId("proyek-222615").setCredentials(ServiceAccountCredentials.fromStream(new FileInputStream("proyek-35e790d3f268.json"))).build().getService();    
      			Query<Entity> query = Query.newEntityQueryBuilder().setKind("user").setOrderBy(OrderBy.asc("namalengkap")).build();
      		    QueryResults<Entity> results = datastore.run(query);
      		    %>
      		  	<div class="navbar-form navbar-left" id="container">
      		  	
      		  	 	<div id="legend">
			      	<legend class="">&nbspCari Informasi User</legend>
			    	</div>
      		  	
      		  	 <nav class="navbar navbar-light bg-light" id="tbobat">
				  <form class="form-inline">
				    <input class="form-control mr-sm-2" type="search" id="tbobat2" name="tbSearch" placeholder="Search User" aria-label="Search">
				    <button class="btn btn-outline-success my-2 my-sm-0" name="btnSearch" type="submit">Cari</button>
				  </form>
				</nav>
      		  	<div id="legend">
			      	<legend class=""></legend>
			    	</div>
      		  	<%
      		    while (results.hasNext()) {
      		        Entity entity = results.next(); 
      		        %>
      		       <!--  /* out.println("Judul Artikel : " + entity.getString("judulartikel") +"<br/>"
                              + " "+"Isi Artikel : "+ entity.getString("isiartikel")+"<br/>"
                              + " "+"Tanggal Insert :"+(entity.getTimestamp("created").toSqlTimestamp().getDate())+"-"+entity.getTimestamp("created").toSqlTimestamp().getMonth()+"-"+(entity.getTimestamp("created").toSqlTimestamp().getYear() + 1900)+"<br/>"
                              + " "+"Kategori: "+ entity.getString("kategori")+"<br/>"
                              + " "+"ID Artikel : "+ entity.getKey().getId()+"<br/>"
                              );     */
                              //listartikel.add(new Artikel(entity.getString("judulartikel"))); -->                       
                       <div id="namaobat"><%=entity.getString("namalengkap")%></div>
                        <div id="namaobat"><%=entity.getString("email")%></div>
                       <br>                               			  			          
                    <%}%>
                    
                    
                    
                    <%-- <%for (int i = 0; i < listartikel.size(); i++) { %>
                    	<h2>Judul Artikel <%=listartikel.get(i).getJudul()%></h2>  
                    	<form action="home.jsp" method="POST">
                    	<button type="button" onclick="window.location.href='home.jsp'" class="btn btn-link" name="btn(<%=i%>)"><h2><%=listartikel.get(i).getJudul()%></h2></button>                 	                   	
                    	</form>
                    	<br>
                    	
                    	<%
                    		/* if(request.getParameter("btn("+i+")")!=null)
                    		{
                    			response.sendRedirect("login.jsp");  
                    		} */
                    		Entity entity = results.next();
                    	%>
                    	
                    <%} %> --%>
                    
                    
                    
      		  		</div>
              <%}%>
  
  
  
    <br>
    <br>
<div class="footer">
  <p>&copy; 2018 RentalMobil <br>
  All Rights Reserved.</p>
</div>

<br>
<p class="pesanreg"></p>
<br>


	
  </body>
</html>