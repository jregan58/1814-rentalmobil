<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<%@ page import = "java.io.FileInputStream" %>
<%@ page import = "java.io.IOException" %>
<%@ page import = "java.io.PrintWriter" %>
<%@ page import = "com.google.auth.oauth2.ServiceAccountCredentials" %>
<%@ page import = "com.google.cloud.Timestamp" %>
<%@ page import = "com.google.cloud.datastore.Datastore" %>
<%@ page import = "com.google.cloud.datastore.DatastoreOptions" %>
<%@ page import = "com.google.cloud.datastore.Entity" %>
<%@ page import = "com.google.cloud.datastore.FullEntity" %>
<%@ page import = "com.google.cloud.datastore.IncompleteKey" %>
<%@ page import = "com.google.cloud.datastore.KeyFactory" %>
<%@ page import = "com.google.cloud.datastore.Query" %>
<%@ page import = "com.google.cloud.datastore.QueryResults" %>
<%@ page import = "com.google.cloud.datastore.StructuredQuery.CompositeFilter" %>
<%@ page import = "com.google.cloud.datastore.StructuredQuery.OrderBy" %>
<%@ page import = "com.google.cloud.datastore.StructuredQuery.PropertyFilter" %>
<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml" lang="en">
  <head>
    <meta http-equiv="content-type" content="application/xhtml+xml; charset=UTF-8" />
    		 <link rel="stylesheet" type="text/css" href="bootstrap.css">
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
  		<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
    <link rel="stylesheet" type="text/css" href="hd.css">

    <title>Rental Mobil</title>
     <div class="header">Rental Mobil</div>
    <ul class="nav nav-tabs" role="tablist">
	  <li><a href="home.jsp">Home</a></li>
      <li><a href="user.jsp">Daftar user</a></li>
	  <li><a href="tanyajawab.jsp">Tanya Jawab</a></li>
	    <li><a href="tambahmobil.jsp">Tambah mobil</a></li>
	  <li><a href="login.jsp" class="navlogin">Login</a></li>
      <li><a href="register.jsp" class="navregister">Register</a></li>
	<li><a href="rental.jsp">Rental</a></li>
	</ul>
	 <%
    try{
     Cookie ck[]=request.getCookies(); %>
     	<%
     	//if(ck[0].getValue()!=""){ 
     		//response.sendRedirect("home.jsp");   	    	
     	//}
     	%>
	     
    <%} 
    catch(Exception e)
    {
    	
    }
    
    %> 
    <br> 
  </head>

  <body>
      <%!
      	String pesan="";
      %>
       <% 
              if (request.getParameter("btnLogin")!=null)
              {
            	  
                  String email = request.getParameter("email");
                  String password = request.getParameter("password");
                  Datastore datastore = DatastoreOptions.newBuilder().setProjectId("proyek-222615").setCredentials(ServiceAccountCredentials.fromStream(new FileInputStream("proyek-35e790d3f268.json"))).build().getService();    
                   
                  Query<Entity> query = Query.newEntityQueryBuilder()
                          .setKind("user")
                          .setFilter(CompositeFilter.and(
                              PropertyFilter.eq("email", email),PropertyFilter.eq("password", password)))                          
                          .build();
                  QueryResults<Entity> results = datastore.run(query);
                  
                  if(results.hasNext()) {
                	  request.setAttribute("email",email);
                      //Entity entity = results.next();
                      /* out.println("Username User : " + entity.getString("username") +"<br/>"
                              + " "+"Password : "+ entity.getString("password")+"<br/>"
                              + " "+"Nama lengkap : "+ entity.getString("namalengkap")+"<br/>"
                              + " "+"Email : "+ entity.getString("email")+"<br/>"
                              );        */ 
                      		pesan = "Login Berhasil !";
                              Cookie ckuser=new Cookie("userlogin",email);//creating cookie object  
                              response.addCookie(ckuser);//adding cookie in the response  
                              ckuser.setMaxAge(60*60);//changing the maximum age to 0 seconds  
                              response.addCookie(ckuser);//adding cookie in the response  
                              
                              response.sendRedirect("home.jsp");  
                  }
                  else
                  {
                	  pesan = "Username dan Password yang Anda Masukkan Salah !";
                  }
                 
                	  
                 
                
              }
              else{
            	  pesan="";
              }
          
          %>
      
  	<form class="form-register" action='login.jsp'>
  <fieldset>
    <div id="legend">
      <legend class="">&nbspLogin Page</legend>
    </div>
    
    <div class="control-group">
      <!-- Username -->
      <label class="control-label"  for="username">Email</label>
      <div class="controls">
        <input type="text" id="email" name="email" placeholder="email" class="input-register">    
        <p class="help-block"></p>   
      </div>
    </div>
    
 
    <div class="control-group">
      <!-- Password-->
      <label class="control-label" for="password">Password</label>
      <div class="controls">
        <input type="password" id="password" name="password" placeholder="Password" class="input-register">
        <p class="help-block"></p>
      </div>
    </div>
 
    
 	
    <div class="control-group">
      <!-- Button -->
      <div class="controls">
        <button class="btn btn-success" name="btnLogin">Login</button>
        <p class="help-block"></p>
      </div>
    </div>
	 <div class="control-group">
		<p class="pesanreg"><%= pesan%></p>
		</div>
  </fieldset>
</form>

<div class="footer">
  <p>&copy; 2018 Rental Mobil <br>
  All Rights Reserved.</p>
</div>


<br>
<p class="pesanreg"></p>
<br>
<br>
<br>


  </body>
</html>